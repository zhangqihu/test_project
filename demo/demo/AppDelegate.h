//
//  AppDelegate.h
//  demo
//
//  Created by 张其虎 on 16/8/9.
//  Copyright © 2016年 zqh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

