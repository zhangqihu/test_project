//
//  main.m
//  demo
//
//  Created by 张其虎 on 16/8/9.
//  Copyright © 2016年 zqh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        // ddd
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
